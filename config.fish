if status is-interactive
    # Commands to run in interactive sessions can go here
abbr --add .a 'cd ~/.config/alacritty/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .b 'cd ~/.local/bin/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .c 'cd ~/.config/ && exa --color=always --group-directories-first'
abbr --add .f 'cd ~/.config/fish/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .l 'cd ~/.local/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .u 'cd ~/.ulocal/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .q 'cd ~/.config/qtile/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .r 'cd ~/.config/ranger/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .s 'cd ~/.local/scripts/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .z 'cd ~/.config/zsh/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add .V 'cd ~/.local/share/images/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add /e 'cd /etc/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add /u 'cd /usr/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add /v 'cd /var/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add d '~/Documents/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add n nvim
abbr --add p '~/Pictures/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add t '~/tmp/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add B '~/Documents/bibliotek/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add D '~/Downloads/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add P '~/Pictures/screenshots/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add V '~/Videos/ && exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add cat bat
abbr --add dogit 'git add . && git commit -a -m "update_$(date +%)" && git push origin master'
abbr --add kalkulator speedcrunch
abbr --add ll 'exa --long --color=always --group-directories-first --group --time-style long-iso'
abbr --add la 'exa --long --all --color=always --group-directories-first --group --time-style long-iso'
abbr --add p1 'ping -c 1 '
abbr --add PO poweroff
abbr --add pr perl-rename
abbr --add pserver 'doas python3 -m http.server 8080'
abbr --add RB reboot
abbr --add RS 'doas systemctl restart lightdm'
abbr --add se sudoedit
abbr --add trp trash-put
abbr --add vall 'cd ~/.config/nvim/; nvim {init.lua,lua/jbm/*.lua}'
abbr --add via 'cd ~/.config/alacritty/ && nvim alacritty.yml'
abbr --add vik 'nvim /home/jan/.local/share/vimwiki/index.md'
abbr --add viq 'cd ~/.config/qtile; cp config.py backup/$(date +%s)_config.py;  nvim config.py'
abbr --add xw 'xwininfo -tree -root'
abbr --add xwg 'xwininfo -tree -root | rg '
abbr --add vr 'virsh snapshot-revert --current win10b'
abbr --add yd 'yt-dlp'
end
